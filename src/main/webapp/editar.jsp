<%-- 
    Document   : index
    Created on : 17-abr-2021, 2:08:10
    Author     : Jonathan
--%>

<%@page import="java.util.List"%>
<%@page import="ciisa.unidad2.entity.Persona"%>
<%@page import="ciisa.dao.PersonaJpaController"%>
<%@page import="modelo.controladordatos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/estilosform.css" type="text/css" media="all" />
        <style>h1 { color: #FFFFFF; }</style>
	<script type="text/javascript" src="js/validaciones.js"></script>
	<title>UNIDAD2</title>
    </head>
    <% Persona resultado = (Persona)request.getAttribute("buscar");%>
   <body>
 <form  name="form" action="controladordatos" method="POST">
    <center>
    <table>
    
<tr><td class="td0" colspan="3"><h1>EDICION</h1></td></tr>
<tr><td class="td1"><label for="run">RUN</label></td><td class="td2"><input type="text" id="rut" name="rut" value="<%=resultado.getRut()%>"></td><td class="td3"></td></tr>
<tr><td class="td1"><label for="nombre">Nombre</label></td><td class="td2"><input type="text" id="nombre" name="nombre" value="<%=resultado.getNombre()%>"></td></tr>
<tr><td class="td1"><label for="apellidop">Apellido Paterno</label></td><td class="td2"><input type="text" id="apellidop" name="apellidop" value="<%=resultado.getApellidoPaterno()%>"></td></tr>
<tr><td class="td1"><label for="apellidom">Apellido Materno</label></td><td class="td2"><input type="text" id="apellidom" name="apellidom" value="<%=resultado.getApellidoMaterno()%>"></td></tr>
<tr><td class="td1"><label for="email">Correo Electrónico</label></td><td class="td2"><input type="email" id="email" name="email" value="<%=resultado.getEmail()%>"></td></tr>

    </table>
    <br>
    <br>
    <button type="submit" class="btn btn-primary" name="accion" value="cancelar">CANCELAR</button>
    <button type="submit" class="btn btn-primary" name="accion" value="aplicar">APLICAR</button>
     </center>
  </form>
</body>
</html>
