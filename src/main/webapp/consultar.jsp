<%-- 
    Document   : index
    Created on : 29-03-2021, 10:45:38
    Author     : Jonathan Ahumada
--%>

<%@page import="java.util.Iterator"%>
<%@page import="ciisa.unidad2.entity.Persona"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       <title>Unidad 2</title>
	<link rel="stylesheet" href="css/estilosform.css" type="text/css" media="all" />
	<script type="text/javascript" src="js/validarradio.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
       
    </head>
    <%
    List<Persona> usuarios = (List<Persona>) request.getAttribute("listaPersona");
    Iterator<Persona> itPersona = usuarios.iterator();
    %>
    
<body>
 <form  name="form" onsubmit="return validacion(); action="controladordatos" method="POST">
           <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            
               <center>
               <table class="table table-bordered" >
                    <thead>
                    <th>Rut</th>
                    <th>Nombre </th>
             
                    <th> Apellido P</th>
                    <th> Apellido M</th>
                    <th> Email</th>
                    <th> Seleccionar</th>
                    </thead>
                    <tbody>
                        <%while (itPersona.hasNext()) {
                       Persona per = itPersona.next();%>
                        <tr>
                            <td><%= per.getRut()%></td>
                            <td><%= per.getNombre()%></td>
                          <td><%= per.getApellidoPaterno()%></td>
                           <td><%= per.getApellidoMaterno()%></td>
                           <td><%= per.getEmail()%></td>
                           <td> <input type="radio" id="radio" name="seleccion" value="<%= per.getRut()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </body>           
                </table>
                    <br>
                    <br>
                    <button type="submit" class="btn btn-primary" name="accion" value="inicio">INICIO</button>
                    <button type="submit" class="btn btn-primary" name="accion" value="borrar">BORRAR</button>
                    <button type="submit" class="btn btn-primary" name="accion" value="editar">EDITAR</button>
              </center>

        
        </form>
</body>
           
</html>
